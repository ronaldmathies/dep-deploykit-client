package nl.sodeso.deploykit.jetty.endpoint;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.commons.restful.model.Message;
import nl.sodeso.deploykit.client.common.exception.DeployException;
import nl.sodeso.deploykit.client.common.model.ProvisionConfiguration;
import nl.sodeso.deploykit.client.common.util.ParametersContainer;
import nl.sodeso.deploykit.jboss.DeployProfile;
import nl.sodeso.deploykit.jboss.cli.JBossCliExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * @author Ronald Mathies
 */
@Path("/deploykit")
public class DeployKitService {

    public static final String ENCODING = "UTF-8";
    public static final String PROVISION_PATH = "provision";

    private static final Log LOG = LogFactory.getLog(DeployKitService.class);

    public DeployKitService() {
    }

    @Context
    private UriInfo headers;

    @POST
    @Path(PROVISION_PATH)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response provision(ProvisionConfiguration configuration) {
        Response.ResponseBuilder response = Response.ok();
        response.encoding(ENCODING);

        PropertyConfiguration.getInstance().addContainer(new ParametersContainer(configuration));

        try {
            DeployProfile.deploy();
            if (JBossCliExecutor.restartJBoss() != JBossCliExecutor.EXIT_NORMAL) {
                return Response.status(Response.Status.BAD_REQUEST).entity(new Message("INF-001", "")).type(MediaType.APPLICATION_JSON_TYPE).build();
            }
        } catch (DeployException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new Message("INF-001", "")).type(MediaType.APPLICATION_JSON_TYPE).build();
        }

        return Response.status(Response.Status.OK).entity(new Message("INF-001", "")).type(MediaType.APPLICATION_JSON_TYPE).build();
    }

}
