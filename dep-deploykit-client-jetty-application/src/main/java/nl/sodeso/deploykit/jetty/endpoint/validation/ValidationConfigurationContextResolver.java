package nl.sodeso.deploykit.jetty.endpoint.validation;

import nl.sodeso.commons.restful.validation.AbstractValidationConfigurationContextResolver;
import nl.sodeso.deploykit.jboss.endpoint.DeployKitService;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class ValidationConfigurationContextResolver extends AbstractValidationConfigurationContextResolver {

    @Override
    protected List<String> getParameterNames(Method method) {
        if (DeployKitService.PROVISION_PATH.equals(method.getName())) {
            return Arrays.asList(
            );
        }

        return null;
    }

    @Override
    protected List<String> getParameterNames(Constructor<?> constructor) {
        return null;
    }

}