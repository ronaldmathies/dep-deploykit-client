package nl.sodeso.deploykit.jetty;

import nl.sodeso.deploykit.jetty.endpoint.DeployKitService;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

/**
 * @author Ronald Mathies
 */
public class DeployKitJettyApplication {

    private static final int HTTP_PORT = 8070;
    private static final String JERSEY_PROVIDER_CLASS_KEY = "jersey.config.server.provider.classnames";

    public static void main(String args[]) {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        Server server = new Server(HTTP_PORT);
        server.setHandler(context);

        ServletHolder servlet = context.addServlet(ServletContainer.class, "/*");
        servlet.setInitOrder(0);
        servlet.setInitParameter(JERSEY_PROVIDER_CLASS_KEY, DeployKitService.class.getCanonicalName());

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            // Do nothing...
        } finally {
            server.destroy();
        }
    }


}