package nl.sodeso.deploykit.jetty.generator;

import nl.sodeso.commons.security.digest.Algorithm;
import nl.sodeso.commons.security.digest.DigestUtil;
import nl.sodeso.commons.security.digest.exception.FailedToCreateDigestException;
import nl.sodeso.deploykit.jetty.util.SystemPropertiesContainer;
import org.junit.BeforeClass;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractGeneratorTest {

    @BeforeClass
    public static void init() {
        String etcPath = System.getProperty("user.dir") + "/build";

        System.setProperty("deploykit.home", System.getProperty("user.dir") + "/src/test/resources");
        System.setProperty(SystemPropertiesContainer.JETTY_HOME, etcPath);

        File etcFile = new File(etcPath + "/etc");
        if (!etcFile.exists()) {
            etcFile.mkdirs();
        }
    }

    protected void compareTwoFiles(String file1, String file2) throws FailedToCreateDigestException {
        String digestFromFile1 = DigestUtil.digest(new File(file1), Algorithm.SHA256);
        String digestFromFile2 = DigestUtil.digest(new File(file2), Algorithm.SHA256);

        assertEquals(digestFromFile1, digestFromFile2);
    }

}
