package nl.sodeso.deploykit.jetty.deploy;

import nl.sodeso.deploykit.jetty.configuration.JettyConfiguration;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.jetty.generator.JettyLdapAuthenticationGenerator;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.model.accesscontrol.LdapAccessControl;

import javax.annotation.Nonnull;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployAuthenticationConfiguration {

    private static final Logger LOG = Logger.getLogger(DeployAuthenticationConfiguration.class.getName());

    public static void deploy(@Nonnull JettyConfiguration configuration, @Nonnull Profile profile) {
        if (profile.getAccessControl() != null &&
                profile.getAccessControl() instanceof LdapAccessControl) {
            LdapAccessControl ldapAccessControl = (LdapAccessControl) profile.getAccessControl();

            LOG.log(Level.INFO, "Deploying authentication configuration '" + ldapAccessControl.getLabel() + "'.");

            JettyXmlFile jettyXmlFile = new JettyXmlFile(ldapAccessControl.getUuid(), "ldap");
            JettyLdapAuthenticationGenerator.generate(ldapAccessControl, jettyXmlFile);
            configuration.addXmlFile(jettyXmlFile);

            LOG.log(Level.INFO, "AccessControl '" + ldapAccessControl.getLabel() + "' successfully added to Jetty configuration, deployment of the authentication configuration is finished.");
        }
    }
}