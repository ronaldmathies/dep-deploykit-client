package nl.sodeso.deploykit.jetty.configuration;

import java.io.File;
import java.util.Objects;

/**
 * @author Ronald Mathies
 */
public class JettyXmlFile {

    private String uuid;
    private String suffix;

    public JettyXmlFile(String xmlFileName) {
        String[] parts = xmlFileName.split("-");
        this.uuid = parts[0];
        this.suffix = parts[1].split(".")[0];
    }

    public JettyXmlFile(String uuid, String suffix) {
        this.uuid = uuid;
        this.suffix = suffix;
    }

    public String getXmlFileName() {
        return this.uuid + "-" + suffix + ".xml";
    }

    public File getXmlFile() {
        return new File(JettyConfUtil.getJettyHomeFolder(), "/etc/" + getXmlFileName());
    }

    public boolean isEqual(String uuid, String suffix) {
        return this.uuid.equals(uuid) && this.suffix.equals(suffix);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JettyXmlFile that = (JettyXmlFile) o;
        return Objects.equals(uuid, that.uuid) &&
                Objects.equals(suffix, that.suffix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, suffix);
    }
}
