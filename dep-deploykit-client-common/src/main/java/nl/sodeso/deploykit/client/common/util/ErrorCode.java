package nl.sodeso.deploykit.client.common.util;

/**
 * @author Ronald Mathies
 */
public enum ErrorCode {

    ERR_000("ERR-000", "An unexpected error occurred: %s."),
    ERR_001("ERR-001", "System environmental variable " + ParametersContainer.DEPLOYKIT_HOST + " was not set."),
    ERR_002("ERR-002", "System environmental variable " + ParametersContainer.DEPLOYKIT_PORT + " was not set."),
    ERR_003("ERR-003", "System environmental variable " + ParametersContainer.PROFILE_NAME + " was not set."),
    ERR_004("ERR-004", "Profile '%s' not found, error message: %s"),
    ERR_005("ERR-005", "Failed to retrieve profile '%s', error message: %s"),
    ERR_006("ERR-006", "Failed to write profile '%s' to profiles folder, error message: %s"),
    ERR_007("ERR-007", "System environmental variable " + SystemPropertiesContainer.DEPLOYKIT_HOME + " was not set."),
    ERR_008("ERR-008", "Failed to add the JDBC driver module '%s'"),
    ERR_009("ERR-009", "Failed to add the JDBC driver '%s'"),
    ERR_010("ERR-010", "Failed to add the data source '%s'"),
    ERR_011("ERR-011", "Failed to create keystore folder '%s'"),
    ERR_012("ERR-012", "System environmental variable " + SystemPropertiesContainer.JBOSS_HOME + " was not set."),
    ERR_013("ERR-013", "Failed to add keystore."),
    ERR_014("ERR-014", "Failed to add security realm."),
    ERR_015("ERR-015", "Failed to modify http connector port."),
    ERR_016("ERR-016", "Failed to modify http connector bound address."),
    ERR_017("ERR-017", "System environmental variable " + ParametersContainer.PROFILE_VERSION + " was not set."),
    ERR_018("ERR-018", "Failed to add data source connection property (data source '%s', key '%s', value '%s'."),
    ERR_019("ERR-019", "System environmental variable " + SystemPropertiesContainer.JBOSS_HOME + " was not set."),
    ERR_020("ERR-020", "Failed to create authentication domain for authentication configuration '%s'."),
    ERR_021("ERR-021", "Failed to create authentication configuration '%s'."),
    ERR_022("ERR-022", "Failed to download artifact (%s)."),
    ERR_023("ERR-023", "Failed to deploy artifact (%s)."),
    ERR_024("ERR-024", "Failed to extract resources (%s).");

    String code;
    String description;

    ErrorCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

}
