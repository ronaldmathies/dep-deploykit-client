package nl.sodeso.deploykit.client.common.model;

import javax.validation.constraints.NotNull;

public class ApplicationConfiguration {

    @NotNull
    private String groupId;

    @NotNull
    private String artifactId;

    @NotNull
    private String version;

    private String classifier;
    private String ext;

    public ApplicationConfiguration() {}

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
