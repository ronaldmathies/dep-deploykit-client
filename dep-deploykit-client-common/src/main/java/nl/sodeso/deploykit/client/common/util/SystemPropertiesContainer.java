package nl.sodeso.deploykit.client.common.util;

import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.annotations.SystemResource;
import nl.sodeso.commons.properties.containers.system.SystemContainer;

/**
 * Properties Container for resolving system properties.
 *
 * @author Ronald Mathies
 */
@Resource(domain="default")
@SystemResource
public class SystemPropertiesContainer extends SystemContainer {

    public static final String DOMAIN = "default";

    public static final String DEPLOYKIT_HOME = "DEPLOYKIT_HOME";
    public static final String JBOSS_HOME = "JBOSS_HOME";
    public static final String JETTY_HOME = "JBOSS_HOME";


}
