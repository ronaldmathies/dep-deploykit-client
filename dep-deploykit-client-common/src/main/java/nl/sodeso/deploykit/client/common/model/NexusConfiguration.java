package nl.sodeso.deploykit.client.common.model;

import javax.validation.constraints.NotNull;

public class NexusConfiguration {

    @NotNull
    String host;

    @NotNull
    String port;

    @NotNull
    String repository;

    public NexusConfiguration() {}

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }
}
