package nl.sodeso.deploykit.client.common.model;

import javax.validation.constraints.NotNull;

public class DeployKitConfiguration {

    @NotNull
    String host;

    @NotNull
    String port;

    public DeployKitConfiguration() {}

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

}
