package nl.sodeso.deploykit.client.common.model;

import javax.validation.constraints.NotNull;

public class ProvisionConfiguration {

    @NotNull
    private ProfileConfiguration profileConfiguration;

    @NotNull
    private DeployKitConfiguration deployKitConfiguration;

    @NotNull
    private NexusConfiguration nexusConfiguration;

    @NotNull
    private ApplicationConfiguration applicationConfiguration;

    @NotNull
    private ResourceConfiguration resourceConfiguration;

    public ProvisionConfiguration() {}

    public ProfileConfiguration getProfileConfiguration() {
        return profileConfiguration;
    }

    public void setProfileConfiguration(ProfileConfiguration profileConfiguration) {
        this.profileConfiguration = profileConfiguration;
    }

    public DeployKitConfiguration getDeployKitConfiguration() {
        return deployKitConfiguration;
    }

    public void setDeployKitConfiguration(DeployKitConfiguration deployKitConfiguration) {
        this.deployKitConfiguration = deployKitConfiguration;
    }

    public NexusConfiguration getNexusConfiguration() {
        return nexusConfiguration;
    }

    public void setNexusConfiguration(NexusConfiguration nexusConfiguration) {
        this.nexusConfiguration = nexusConfiguration;
    }

    public ApplicationConfiguration getApplicationConfiguration() {
        return applicationConfiguration;
    }

    public void setApplicationConfiguration(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;
    }

    public ResourceConfiguration getResourceConfiguration() {
        return resourceConfiguration;
    }

    public void setResourceConfiguration(ResourceConfiguration resourceConfiguration) {
        this.resourceConfiguration = resourceConfiguration;
    }
}
