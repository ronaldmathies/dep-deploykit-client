package nl.sodeso.deploykit.client.common.util;

import nl.sodeso.commons.properties.containers.memory.MemoryContainer;
import nl.sodeso.deploykit.client.common.model.ProvisionConfiguration;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ronald Mathies
 */
public class ParametersContainer extends MemoryContainer {

    public static final String DOMAIN = "parameters";
    public static final String DEPLOYKIT_HOST = "deploykit_host";
    public static final String DEPLOYKIT_PORT = "deploykit_port";
    public static final String NEXUS_HOST = "nexus_host";
    public static final String NEXUS_PORT = "nexus_port";
    public static final String NEXUS_REPOSITORY = "nexus_repository";
    public static final String PROFILE_NAME = "profile_name";
    public static final String PROFILE_VERSION = "profile_version";
    public static final String APP_GROUPID = "app_groupid";
    public static final String APP_ARTIFACTID = "app_artifactid";
    public static final String APP_VERSION = "app_version";
    public static final String APP_CLASSIFIER = "app_classifier";
    public static final String APP_EXTENSION = "app_ext";
    public static final String RES_GROUPID = "res_groupid";
    public static final String RES_ARTIFACTID = "res_artifactid";
    public static final String RES_VERSION = "res_version";
    public static final String RES_CLASSIFIER = "res_classifier";
    public static final String RES_EXTENSION = "res_ext";

    public ParametersContainer(ProvisionConfiguration configuration) {
        super(DOMAIN, configurationToMap(configuration));
    }

    private static Map<String, String> configurationToMap(ProvisionConfiguration configuration) {
        Map<String, String> properties = new HashMap<>();
        properties.put(DEPLOYKIT_HOST, configuration.getDeployKitConfiguration().getHost());
        properties.put(DEPLOYKIT_PORT, configuration.getDeployKitConfiguration().getPort());
        properties.put(NEXUS_HOST, configuration.getNexusConfiguration().getHost());
        properties.put(NEXUS_PORT, configuration.getNexusConfiguration().getPort());
        properties.put(NEXUS_REPOSITORY, configuration.getNexusConfiguration().getRepository());
        properties.put(PROFILE_NAME, configuration.getProfileConfiguration().getName());
        properties.put(PROFILE_VERSION, configuration.getProfileConfiguration().getVersion());
        properties.put(APP_GROUPID, configuration.getApplicationConfiguration().getGroupId());
        properties.put(APP_ARTIFACTID, configuration.getApplicationConfiguration().getArtifactId());
        properties.put(APP_VERSION, configuration.getApplicationConfiguration().getVersion());
        properties.put(APP_CLASSIFIER, configuration.getApplicationConfiguration().getClassifier());
        properties.put(APP_EXTENSION, configuration.getApplicationConfiguration().getExt());
        properties.put(RES_GROUPID, configuration.getResourceConfiguration().getGroupId());
        properties.put(RES_ARTIFACTID, configuration.getResourceConfiguration().getArtifactId());
        properties.put(RES_VERSION, configuration.getResourceConfiguration().getVersion());
        properties.put(RES_CLASSIFIER, configuration.getResourceConfiguration().getClassifier());
        properties.put(RES_EXTENSION, configuration.getResourceConfiguration().getExt());

        return properties;
    }

}
