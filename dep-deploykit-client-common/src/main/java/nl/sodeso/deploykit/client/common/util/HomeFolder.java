package nl.sodeso.deploykit.client.common.util;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.client.common.exception.DeployException;

import java.io.File;

/**
 * Created by sodeso on 08/03/16.
 */
public class HomeFolder {

    public static File getProfilesFolder() {
        File deploykitHomeFolder = PropertyConfiguration.getInstance().getFileProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.DEPLOYKIT_HOME);
        if (deploykitHomeFolder == null) {
            throw new DeployException(ErrorCode.ERR_007);
        }

        File file = new File(deploykitHomeFolder, "/bin/profiles/");
        if (!file.exists()) {
            file.mkdirs();
        }

        return file;
    }

}
