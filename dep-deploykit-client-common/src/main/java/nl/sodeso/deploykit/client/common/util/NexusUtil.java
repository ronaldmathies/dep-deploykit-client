package nl.sodeso.deploykit.client.common.util;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.client.common.util.ErrorCode;
import nl.sodeso.deploykit.client.common.exception.DeployException;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

import static nl.sodeso.deploykit.client.common.util.ParametersContainer.*;

/**
 * A utility class to
 * @author Ronald Mathies
 */
public class NexusUtil {

    public static File resolveArtifact(String groupId, String artifactId, String version, String packaging, String classifier, String extension) {
        String nexusHost = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, NEXUS_HOST);
        String nexusPort = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, NEXUS_PORT);
        String nexusRepository = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, NEXUS_REPOSITORY);

        InputStream input = null;
        OutputStream output = null;

        String nexusDownloadUrl = String.format("%s:%s/service/local/artifact/maven/content?g=%s&a=%s&v=%s&r=%s",
                nexusHost, nexusPort, groupId, artifactId, version, nexusRepository);

        if (packaging != null) {
            nexusDownloadUrl = String.format("%s&p=%s", nexusDownloadUrl, classifier);
        }

        if (classifier != null) {
            nexusDownloadUrl = String.format("%s&c=%s", nexusDownloadUrl, classifier);
        }

        if (extension != null) {
            nexusDownloadUrl = String.format("%s&e=%s", nexusDownloadUrl, extension);
        }

        try {
            URL url = new URL(nexusDownloadUrl);
            URLConnection connection = url.openConnection();

            input = connection.getInputStream();
            byte[] buffer = new byte[4096];
            int size;

            String artifact = String.format("%s-%s", artifactId, version);
            if (classifier != null) {
                artifact = String.format("%s-%s", artifact, classifier);
            }
            if (extension != null) {
                artifact = String.format("%s.%s", artifact, extension);
            }

            File deployable = new File(HomeFolder.getProfilesFolder(), artifact);

            output = new FileOutputStream(deployable);
            while ((size = input.read(buffer)) != -1) {
                output.write(buffer, 0, size);
            }

            output.close();

            return deployable;
        } catch (IOException e) {
            throw new DeployException(ErrorCode.ERR_022, e.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    // Ignore.
                }
            }

            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    // Ignore.
                }
            }
        }
    }

}
