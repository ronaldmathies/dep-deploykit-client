package nl.sodeso.deploykit.client.common.exception;

import nl.sodeso.deploykit.client.common.util.ErrorCode;

import javax.annotation.Nonnull;

/**
 * @author Ronald Mathies
 */
public class DeployException extends RuntimeException {

    /**
     * Constructs a DeployException.
     * @param code the error code.
     * @param args the arguments for the message.
     */
    public DeployException(@Nonnull ErrorCode code, String ... args) {
        super(code.getCode() + " - " + String.format(code.getDescription(), (Object[])args));
    }

}
