package nl.sodeso.deploykit.client.common.model;

import javax.validation.constraints.NotNull;

/**
 * Created by sodeso on 07/03/16.
 */
public class ProfileConfiguration {

    @NotNull
    String name;

    @NotNull
    String version;

    public ProfileConfiguration() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
