package nl.sodeso.deploykit.client.common.util;

import nl.sodeso.deploykit.client.common.exception.DeployException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author Ronald Mathies
 */
public class ZipUtil {

    public static void unzip(File zipFile, File outputFolder){
        byte[] buffer = new byte[4096];

        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile))) {
            ZipEntry entry = zis.getNextEntry();

            while (entry != null) {
                File newFile = new File(outputFolder + File.separator + entry.getName());
                new File(newFile.getParent()).mkdirs();

                try (FileOutputStream fos = new FileOutputStream(newFile)) {

                    int size;
                    while ((size = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, size);
                    }

                } catch (IOException ex) {
                    throw new DeployException(ErrorCode.ERR_024, ex.getMessage());
                }

                entry = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();
        } catch(IOException ex) {
            throw new DeployException(ErrorCode.ERR_024, ex.getMessage());
        }
    }

}
