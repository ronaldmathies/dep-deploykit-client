docker stop deploykit-jetty
docker rm deploykit-jetty
docker rmi deploykit/jetty

if [ -f resources.tar.gz ];
then
    echo removing existing resources container.
    rm resources.tar.gz
fi

echo creating resources container.
tar -zcvf resources.tar.gz resources/

docker build -t deploykit/jetty .
docker run -d -p 7080:8080 --name deploykit-jetty deploykit/jetty
docker exec -t -i deploykit bash