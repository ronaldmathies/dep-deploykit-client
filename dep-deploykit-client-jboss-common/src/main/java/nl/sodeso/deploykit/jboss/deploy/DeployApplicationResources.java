package nl.sodeso.deploykit.jboss.deploy;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.commons.token.TokenReplacingReader;
import nl.sodeso.deploykit.client.common.exception.DeployException;
import nl.sodeso.deploykit.client.common.util.*;
import nl.sodeso.deploykit.service.model.Profile;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

import static nl.sodeso.deploykit.client.common.util.ParametersContainer.*;

/**
 * @author Ronald Mathies
 */
public class DeployApplicationResources {

    private static final Logger LOG = Logger.getLogger(DeployApplicationResources.class.getName());

    private static final String MODULE_XML =
            "<?xml version=\"1.0\"?>\n" +
            "<module xmlns=\"urn:jboss:module:1.1\" name=\"application.resources\">\n" +
            "   <resources>\n" +
            "       <resource-root path=\".\"/>\n" +
            "   </resources>\n" +
            "</module>";

    /**
     * Deploys the application resources to a JBoss module.
     * @param profile the profile.
     */
    public static void deploy(@Nonnull  Profile profile) {
        String resGroupId = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, RES_GROUPID);
        String resArtifactId = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, RES_ARTIFACTID);
        String resVersion = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, RES_VERSION);
        String resClassifier = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, RES_CLASSIFIER);
        String resExtension = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, RES_EXTENSION);

        File downloadedResources = NexusUtil.resolveArtifact(resGroupId, resArtifactId, resVersion, null, resClassifier, resExtension);
        if (downloadedResources != null) {
            File resources = getDeployKitResourcesFolder();
            ZipUtil.unzip(downloadedResources, resources);
            loopFilesInFolder(resources, profile);
            createJBossModuleXmlFile();
        }
    }

    /**
     * Loops through all the resources recursivly and processes every single file found.
     * @param folder the folder containing all the resources.
     * @param profile the profile containing all the placeholders.
     */
    private static void loopFilesInFolder(@Nonnull  File folder, @Nonnull Profile profile) {
        try {
            Files.walkFileTree(folder.toPath(), new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    processResource(file.toFile(), profile);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            // Do nothing.
        }
    }

    /**
     * Creates the JBoss module XML file.
     */
    private static void createJBossModuleXmlFile() {
        LOG.log(Level.INFO, "Creating JBoss module configuration");

        File jbossModuleFolder = getJBossModuleFolder();
        try {
            File file = new File(jbossModuleFolder, "module.xml");
            LOG.log(Level.INFO, "Writing JBoss module configuration: " + file.getPath());
            Files.write(file.toPath(), MODULE_XML.getBytes());
        } catch (IOException e) {
            // Do nothing at this moment.
        }
    }

    /**
     * Processes a single resource file (replaces all placeholders with their actual value), and it will write it to the
     * destination folder within the JBoss modules folder along with the reconstructed path.
     *
     * @param resource the resource to process.
     * @param profile the profile containing all the placeholders that are available.
     */
    private static void processResource(@Nonnull File resource, @Nonnull Profile profile) {
        LOG.log(Level.INFO, "Processing resource: " + resource.getPath());

        OutputStreamWriter writer = null;

        try (TokenReplacingReader reader = new TokenReplacingReader(new InputStreamReader(new FileInputStream(resource)), profile)) {
            // Get the base folder where the resources are located, this will be used to
            // determine which part of the path should be reconstructed within the module.
            Path userdir = new File(System.getProperty("user.dir"), "resources").toPath();

            // Get the path that should be reconstructed within the module (basicly it does
            // a substraction of the working path on the jboss-client side with the additional
            // path within the resource.
            String workingfolder = userdir.relativize(resource.toPath()).toString();

            // Construct the new path based on the JBoss module folder and the additional path
            // constructed before. If the constructed path is empty then just use the JBoss module folder.
            File processedResourceFile;
            if (workingfolder != null && !workingfolder.isEmpty()) {
                processedResourceFile = new File(getJBossModuleFolder(), workingfolder);
            } else {
                processedResourceFile = getJBossModuleFolder();
            }

            // Check if the newly constructed path exists, if not create the folders recursivly.
            if (!processedResourceFile.getParentFile().exists()) {
                processedResourceFile.getParentFile().mkdirs();
            }

            // Process and write the resource to the destination path.
            LOG.log(Level.INFO, "Writing processed resource: " + processedResourceFile.toString());
            writer = new OutputStreamWriter(new FileOutputStream(processedResourceFile));

            int size;
            char[] buffer = new char[2048];
            while (-1 != (size = reader.read(buffer))) {
                writer.write(buffer, 0, size);
            }

            writer.flush();
        } catch (IOException e) {
            // Do nothing yet...
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    // Do nothing.
                }
            }
        }
    }

    /**
     * Returns the resources folder within the DeployKit bin folder.
     * @return the resources folder within the DeployKit bin folder.
     */
    private static File getDeployKitResourcesFolder() {
        File deploykitHomeFolder = PropertyConfiguration.getInstance().getFileProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.DEPLOYKIT_HOME);
        if (deploykitHomeFolder == null) {
            throw new DeployException(ErrorCode.ERR_007);
        }

        return new File(deploykitHomeFolder, "/bin/resources/");
    }

    /**
     * Returns the JBoss module folder with the application resources folder within it, if it doesn't exist it will
     * be created.
     *
     * @return the JBoss module folder with the application resources folder within it.
     */
    private static File getJBossModuleFolder() {
        File jbossHomeFolder = PropertyConfiguration.getInstance().getFileProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.JBOSS_HOME);
        if (jbossHomeFolder == null) {
            throw new DeployException(ErrorCode.ERR_019);
        }

        File moduleFolder = new File(jbossHomeFolder, "/modules/application/resources/main/");
        if (!moduleFolder.exists()) {
            moduleFolder.mkdirs();
        }

        return moduleFolder;
    }
}
