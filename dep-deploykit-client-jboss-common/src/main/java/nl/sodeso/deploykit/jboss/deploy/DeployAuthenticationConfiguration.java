package nl.sodeso.deploykit.jboss.deploy;

import nl.sodeso.deploykit.client.common.exception.DeployException;
import nl.sodeso.deploykit.client.common.util.ErrorCode;
import nl.sodeso.deploykit.jboss.cli.JBossCliExecutor;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.model.accesscontrol.DatasourceAccessControl;
import nl.sodeso.deploykit.service.model.accesscontrol.LdapAccessControl;

import javax.annotation.Nonnull;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployAuthenticationConfiguration {

    private static final Logger LOG = Logger.getLogger(DeployDatasourceConfiguration.class.getName());

    private static final String CLI_ADD_AUTHENTICATION_DOMAIN =
            "/subsystem=security/security-domain=%s:add(cache-type=default)";

    private static final String CLI_ADD_AUTHENTICATION_LDAP =
            "/subsystem=security/security-domain=%s/authentication=classic:add(" +
            "login-modules=[" +
            "{code=Ldap,flag=optional," +
            "module-options=>[" +
            "java.naming.factory.initial=com.sun.jndi.ldap.LdapCtxFactory," +
            "java.naming.provider.url=ldap://%s:%d," +
            "java.naming.security.authentication=simple," +
            "java.naming.security.principal=\"%s\"," +
            "java.naming.security.credentials=\"%s\"," +
            "principalDNPrefix=\"%s=\"," +
            "principalDNSuffix=\",%s\"," +
            "rolesCtxDN=\"%s\"," +
            "uidAttributeID=%s," +
            "matchOnUserDN=true," + // Should always be true.
            "allowEmptyPasswords=false," + // Should always be false (no anonymous login).
            "roleAttributeID=%s," +
            "roleAttributeIsDN=false," +
            "searchTimeLimit=5000," +  // By default 5000
            "searchScope=SUBTREE_SCOPE" + // By default SUBTREE_SCOPE
            "]" +
            "}" +
            "])";

    private static final String CLI_ADD_AUTHENTICATION_DATASOURCE =
        "/subsystem=security/security-domain=%s/authentication=classic:add(" + // dukes-forest
        "login-modules=[ " +
        "{code=org.jboss.security.auth.spi.DatabaseServerLoginModule," +
        "flag=required," +
        "module-options=>[" +
        "dsJndiName=java:/%s," + // jboss/ForestXADS
        "principalsQuery=\"%s\"," + // select PASSWORD from forest.PERSON where EMAIL=?
        "rolesQuery=\"%s\"," + // select NAME as 'ROLES', 'Roles' as 'ROLEGROUP' from forest.GROUPS g inner join forest.PERSON_GROUPS pg on g.ID = pg.GROUPS_ID join forest.PERSON p on p.EMAIL = pg.EMAIL where p.EMAIL = ?
        "hashAlgorithm=MD5," +
        "hashEncoding=HEX," +
        "]" +
        "}" +
        "])";

    private static final String CLI_ADD_AUTHORIZATION_DATASOURCE =
        "/subsystem=security/security-domain=%s/authorization=classic:add(" + // dukes-forest
        "policy-modules=[ " +
        "{code=org.jboss.security.auth.spi.DatabaseServerLoginModule," +
        "flag=required," +
        "module-options=>[" +
        "dsJndiName=java:/%s," + // jboss/ForestXADS
        "principalsQuery=\"%s\"," + // select PASSWORD from forest.PERSON where EMAIL=?
        "rolesQuery=\"%s\"," + // select NAME as 'ROLES', 'Roles' as 'ROLEGROUP' from forest.GROUPS g inner join forest.PERSON_GROUPS pg on g.ID = pg.GROUPS_ID join forest.PERSON p on p.EMAIL = pg.EMAIL where p.EMAIL = ?
        "hashAlgorithm=MD5," + // Allowed values MD5, SHA-1, SHA-256
        "hashEncoding=HEX," + // Allowed values HEX or base64
        "]" +
        "}" +
        "])";

    public static void deploy(@Nonnull Profile profile) {
        if (profile.getAccessControl() != null) {
            LOG.log(Level.INFO, "Deploying authentication configuration '" + profile.getAccessControl().getLabel() + "'.");

            String addAuthenticationDomain = String.format(CLI_ADD_AUTHENTICATION_DOMAIN, profile.getAccessControl().getRealm());
            if (JBossCliExecutor.execute(addAuthenticationDomain) != JBossCliExecutor.EXIT_NORMAL) {
                throw new DeployException(ErrorCode.ERR_020, profile.getAccessControl().getLabel());
            }

            if (profile.getAccessControl() instanceof LdapAccessControl) {
                LdapAccessControl ldapAccessControl = (LdapAccessControl) profile.getAccessControl();

                String addAuthenticationLdap = String.format(CLI_ADD_AUTHENTICATION_LDAP,
                        ldapAccessControl.getRealm(),
                        ldapAccessControl.getLdapService().getDomain(),
                        ldapAccessControl.getLdapService().getPort(),
                        ldapAccessControl.getLdapService().getUsername(),
                        ldapAccessControl.getLdapService().getPassword(),
                        ldapAccessControl.getUserAttribute(),
                        ldapAccessControl.getUserBaseDn(),
                        ldapAccessControl.getRoleBaseDn(),
                        ldapAccessControl.getRoleMemberAttribute(),
                        ldapAccessControl.getRoleNameAttribute()
                );

                if (JBossCliExecutor.execute(addAuthenticationLdap) != JBossCliExecutor.EXIT_NORMAL) {
                    throw new DeployException(ErrorCode.ERR_021, ldapAccessControl.getLabel());
                }

            } else if (profile.getAccessControl() != null &&
                    profile.getAccessControl() instanceof DatasourceAccessControl) {
                DatasourceAccessControl datasourceAccessControl = (DatasourceAccessControl) profile.getAccessControl();

                String principalsQuery =
                        String.format("SELECT %s FROM %s WHERE %s = ?",
                                datasourceAccessControl.getCredentialColumn(),
                                datasourceAccessControl.getUserTable(),
                                datasourceAccessControl.getUserColumn());

                String rolesQuery = String.format("SELECT %s FROM %s WHERE %s = ?",
                        datasourceAccessControl.getRoleRoleColumn(),
                        datasourceAccessControl.getRoleTable(),
                        datasourceAccessControl.getRoleUserColumn());

                String addAuthenticationDatasource = String.format(CLI_ADD_AUTHENTICATION_DATASOURCE,
                        datasourceAccessControl.getRealm(),
                        datasourceAccessControl.getDatasource().getJndi(),
                        principalsQuery,
                        rolesQuery);
                if (JBossCliExecutor.execute(addAuthenticationDatasource) != JBossCliExecutor.EXIT_NORMAL) {
                    throw new DeployException(ErrorCode.ERR_021, datasourceAccessControl.getLabel());
                }

                String addAuthorizationDatasource = String.format(CLI_ADD_AUTHORIZATION_DATASOURCE,
                        datasourceAccessControl.getRealm(),
                        datasourceAccessControl.getDatasource().getJndi(),
                        principalsQuery,
                        rolesQuery);
                if (JBossCliExecutor.execute(addAuthorizationDatasource) != JBossCliExecutor.EXIT_NORMAL) {
                    throw new DeployException(ErrorCode.ERR_021, datasourceAccessControl.getLabel());
                }
            }

            LOG.log(Level.INFO, "AccessControl '" + profile.getAccessControl().getLabel() + "' successfully added to JBoss / Wildfly configuration, deployment of the authentication configuration is finished.");
        }
    }


    /*
                <security-domain name="realm" cache-type="default">
                    <authentication>
                        <login-module code="Ldap" flag="optional">
                            <module-option name="java.naming.factory.initial" value="com.sun.jndi.ldap.LdapCtxFactory"/>
                            <module-option name="java.naming.provider.url" value="ldap://192.168.99.100:389"/>
                            <module-option name="java.naming.security.authentication" value="simple"/>

                            <module-option name="principalDNPrefix" value="uid="/>
                            <module-option name="principalDNSuffix" value=",ou=users,dc=example,dc=com"/>
                            <module-option name="rolesCtxDN" value=",ou=roles,dc=example,dc=com"/>
                            <module-option name="uidAttributeID" value="member"/>
                            <module-option name="matchOnUserDN" value="true"/>
                            <module-option name="roleAttributeID" value="cn"/>
                            <module-option name="roleAttributeIsDN" value="false"/>
                            <module-option name="searchTimeLimit" value="5000"/>
                            <module-option name="searchScope" value="SUBTREE_SCOPE"/>
                        </login-module>
                    </authentication>
                </security-domain>
     */
}
