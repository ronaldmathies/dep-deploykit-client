package nl.sodeso.deploykit.jboss.deploy;

import nl.sodeso.commons.fileutils.FileUtil;
import nl.sodeso.deploykit.client.common.exception.DeployException;
import nl.sodeso.deploykit.client.common.util.ErrorCode;
import nl.sodeso.deploykit.jboss.cli.JBossCliExecutor;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.model.Resource;
import nl.sodeso.deploykit.service.model.accesscontrol.DatasourceAccessControl;
import nl.sodeso.deploykit.service.model.datasource.connectionpool.JBossConnectionPool;
import nl.sodeso.deploykit.service.model.datasource.datasource.ConnectionProperty;
import nl.sodeso.deploykit.service.model.datasource.datasource.Datasource;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriver;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriverJar;
import org.apache.commons.codec.binary.Base64InputStream;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @link https://developer.jboss.org/wiki/ConfigDataSources?_sscc=t
 *
 * @author Ronald Mathies
 */
public class DeployDatasourceConfiguration {

    private static final Logger LOG = Logger.getLogger(DeployDatasourceConfiguration.class.getName());

    private static final String ENCODING = "UTF-8";

    private static final String CLI_ADD_MODULE =
            "module add " +
            "--name=%s " +
            "--resources=%s " +
            "--dependencies=javax.api,javax.transaction.api,sun.jdk";

    private static final String CLI_ADD_JDBC_DRIVER =
            "/subsystem=datasources/jdbc-driver=%s" +
            ":add(" +
            "driver-name=%s" +
            ",driver-module-name=%s" +
            ",driver-datasource-class-name=%s)"; // TODO: depends on type of class, XA transactions?

    private static final String CLI_ADD_DATASOURCE =
            "data-source add " +
            "--name=%s " +
            "--driver-name=%s " +
            "--jndi-name=java:/%s " +
            "--connection-url=%s " +
            "--user-name=%s " +
            "--password=%s ";

    private static final String CLI_ADD_CONNECTION_PROPERTY =
            "/subsystem=datasources/data-source=%s/connection-properties=%s:add(value=%s)";

    private static final String CLI_ADD_DATASOURCE_POOL =
            "%s" +
            "--min-pool-size=%d " +
            "--max-pool-size=%d " +
            "--pool-prefill=%s " +
            "--allocation-retry=%d " +
            "--allocation-retry-wait-millis=%d " +
            "--blocking-timeout-wait-millis=%d " +
            "--idle-timeout-minutes=%d " +
            "--initial-pool-size=%d ";

    private static final String CLI_ADD_DATASOURCE_POOL_NEW_CONNECTION_SQL =
            "%s --new-connection-sql=\"%s\" ";

    private static final String CLI_ADD_DATASOURCE_POOL_CHECK_VALID_CONNECTION =
            "%s --check-valid-connection-sql=\"%s\" ";

    private static final String CLI_ADD_DATASOURCE_POOL_TRANSACTION_ISOLATION =
            "%s --transaction-isolation=%s ";

    private static List<JdbcDriver> previouslyCreatedJdbcDriverModules = new ArrayList<>();

    public static void deploy(@Nonnull Profile profile) {
        try {
            for (Datasource datasource : profile.getDatasources()) {
                deploy(datasource);
            }

            if (profile.getAccessControl() != null && profile.getAccessControl() instanceof DatasourceAccessControl) {
                DatasourceAccessControl datasourceAccessControl = (DatasourceAccessControl)profile.getAccessControl();

                Resource resource = profile.findResourceByUuid(datasourceAccessControl.getDatasource().getUuid(), Datasource.class);
                if (resource == null) {
                    deploy(datasourceAccessControl.getDatasource());
                }

            }
        } catch (IOException e) {
            throw new DeployException(ErrorCode.ERR_000, e.getMessage());
        }
    }

    private static void deploy(@Nonnull Datasource datasource) throws IOException {
        LOG.log(Level.INFO, "Deploying datasource '" + datasource.getLabel() + "'.");

        String driverName = filter(datasource.getJdbcDriver().getLabel()) + "-DRIVER";

        // Perform a check to see if we have deployed the JDBC driver while processing a
        // previously data source. If so, then don't add it again since we can re-use the other one.
        if (previouslyCreatedJdbcDriverModules.contains(datasource.getJdbcDriver())) {
            List<String> jarFiles = new ArrayList<>();
            for (JdbcDriverJar jdbcDriverJar : datasource.getJdbcDriver().getJdbcDriverJars()) {

                // Create a temporarily file with the contents of the jar file.
                File jdbcDriverJarFile = new File(FileUtil.getSystemTempFolderAsFile(), jdbcDriverJar.getFilename());
                ByteArrayInputStream inputStream = new ByteArrayInputStream(jdbcDriverJar.getJar().getBytes(Charset.forName(ENCODING)));
                Files.copy(new Base64InputStream(inputStream, false), jdbcDriverJarFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

                jarFiles.add(jdbcDriverJarFile.toString());

            }

            // Construct the add module CLI command.
            String moduleName = filter(datasource.getJdbcDriver().getLabel()) + "-MODULE";
            String addJdbcDriverModule = String.format(CLI_ADD_MODULE,
                    moduleName,
                    join(jarFiles, ",")
            );
            if (JBossCliExecutor.execute(addJdbcDriverModule) != JBossCliExecutor.EXIT_NORMAL) {
                throw new DeployException(ErrorCode.ERR_008, moduleName);
            }

            // Construct the add JDBC driverName CLI command.
            String addJdbcDriver = String.format(CLI_ADD_JDBC_DRIVER,
                    driverName,
                    driverName,
                    moduleName,
                    datasource.getJdbcDriver().getJdbcDriverClass()
            );
            if (JBossCliExecutor.execute(addJdbcDriver) != JBossCliExecutor.EXIT_NORMAL) {
                throw new DeployException(ErrorCode.ERR_009, driverName);
            }

            previouslyCreatedJdbcDriverModules.add(datasource.getJdbcDriver());
        }

        // Construct the add datasource CLI command.
        String datasourceName = filter(datasource.getLabel()) + "-DS";
        String addJdbcDriverDatasource = String.format(CLI_ADD_DATASOURCE,
                datasourceName,
                driverName,
                datasource.getJndi(),
                datasource.getUrl(),
                datasource.getUsername(),
                datasource.getPassword());

        if (datasource.getJBossConnectionPool() != null) {
            JBossConnectionPool pool = datasource.getJBossConnectionPool();
            addJdbcDriverDatasource = String.format(CLI_ADD_DATASOURCE_POOL,
                    addJdbcDriverDatasource,
                    pool.getMinPoolSize(),
                    pool.getMaxPoolSize(),
                    pool.getPrefill().toString(),
                    pool.getAllocationRetry(),
                    pool.getAllocationRetryWaitMillis(),
                    pool.getBlockingTimeoutMillis(),
                    pool.getIdleTimeoutMinutes(),
                    pool.getInitialPoolSize());

            if (pool.getConnectionInitSqls() != null && !pool.getConnectionInitSqls().isEmpty()) {
                addJdbcDriverDatasource = String.format(CLI_ADD_DATASOURCE_POOL_NEW_CONNECTION_SQL, addJdbcDriverDatasource, pool.getConnectionInitSqls());
            }

            if (pool.getValidationQuery() != null && !pool.getValidationQuery().isEmpty()) {
                addJdbcDriverDatasource = String.format(CLI_ADD_DATASOURCE_POOL_CHECK_VALID_CONNECTION, addJdbcDriverDatasource, pool.getValidationQuery());
            }

            if (pool.getDefaultTransactionIsolation() != null && !pool.getDefaultTransactionIsolation().isEmpty()) {
                String value = null;
                switch (pool.getDefaultTransactionIsolation()) {
                    case "READ_COMMITED":
                        value = "TRANSACTION_READ_COMMITTED";
                        break;
                    case "READ_UNCOMMITTED":
                        value = "TRANSACTION_READ_UNCOMMITTED";
                        break;
                    case "REPEATABLE_READ":
                        value = "TRANSACTION_REPEATABLE_READ";
                        break;
                    case "SERIALIZABLE":
                        value = "TRANSACTION_SERIALIZABLE";
                        break;
                }

                addJdbcDriverDatasource = String.format(CLI_ADD_DATASOURCE_POOL_TRANSACTION_ISOLATION, addJdbcDriverDatasource, value);
            }
        }
        if (JBossCliExecutor.execute(addJdbcDriverDatasource) != JBossCliExecutor.EXIT_NORMAL) {
            throw new DeployException(ErrorCode.ERR_010, datasourceName);
        }

        if (datasource.getConnectionProperties() != null) {
            for (ConnectionProperty connectionProperty : datasource.getConnectionProperties()) {
                String addDatasourceConnectionProperty = String.format(CLI_ADD_CONNECTION_PROPERTY,
                        datasourceName,
                        connectionProperty.getKey(),
                        connectionProperty.getValue());

                if (JBossCliExecutor.execute(addDatasourceConnectionProperty) != JBossCliExecutor.EXIT_NORMAL) {
                    throw new DeployException(ErrorCode.ERR_018, datasourceName, connectionProperty.getKey(), connectionProperty.getValue());
                }
            }
        }


        LOG.log(Level.INFO, "Datasource '" + datasourceName + "' successfully added to Jetty configuration, deployment of the datasource is finished.");
    }

    /**
     * Filters a label so that it only consists of the letters a-z, A-Z and 0-9.
     * @param label the label.
     * @return the resulting string.
     */
    public static String filter(@Nonnull String label) {
        return label.replaceAll("[^a-zA-Z0-9]", "");
    }

    /**
     * Combines all strings together with the specified the seperator.
     * @param list the list of string to join.
     * @param seperator the seperator to use for combining the string.
     * @return the combined string.
     */
    private static String join(@Nonnull List<?> list, @Nonnull String seperator) {
        StringBuilder join = new StringBuilder();
        for (Object object : list) {
            if (join.length() > 0) {
                join.append(seperator);
            }

            join.append(object.toString());
        }

        return join.toString();
    }
}
