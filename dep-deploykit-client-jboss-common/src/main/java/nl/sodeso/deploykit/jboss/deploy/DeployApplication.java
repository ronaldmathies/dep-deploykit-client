package nl.sodeso.deploykit.jboss.deploy;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.client.common.exception.DeployException;
import nl.sodeso.deploykit.client.common.util.ErrorCode;
import nl.sodeso.deploykit.client.common.util.NexusUtil;
import nl.sodeso.deploykit.jboss.cli.JBossCliExecutor;
import nl.sodeso.deploykit.service.model.Profile;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.logging.*;

import static nl.sodeso.deploykit.client.common.util.ParametersContainer.*;


/**
 * @author Ronald Mathies
 */
public class DeployApplication {

    private static final Logger LOG = Logger.getLogger(DeployApplication.class.getName());

    private static final String CLI_DEPLOY_APPLICATION =
            "deploy %s --force";

    public static void deploy(@Nonnull Profile profile) {
        String appGroupId = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, APP_GROUPID);
        String appArtifactId = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, APP_ARTIFACTID);
        String appVersion = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, APP_VERSION);
        String appClassifier = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, APP_CLASSIFIER);
        String appExtension = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, APP_EXTENSION);

        File downloadedDeployable = NexusUtil.resolveArtifact(appGroupId, appArtifactId, appVersion, null, appClassifier, appExtension);
        if (downloadedDeployable != null) {
            LOG.log(Level.INFO, String.format("Deploying application %s:%s:%s", appGroupId, appArtifactId, appVersion));

            String modifyHttp = String.format(CLI_DEPLOY_APPLICATION, downloadedDeployable.getAbsolutePath());
            if (JBossCliExecutor.execute(modifyHttp) != JBossCliExecutor.EXIT_NORMAL) {
                throw new DeployException(ErrorCode.ERR_023, "Error while executing CLI");
            }

            LOG.log(Level.INFO, String.format("Succesfully deployed application %s:%s:%s", appGroupId, appArtifactId, appVersion));
        }
    }

}
