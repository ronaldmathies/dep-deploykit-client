package nl.sodeso.deploykit.jboss;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.client.common.exception.DeployException;
import nl.sodeso.deploykit.client.common.util.ErrorCode;
import nl.sodeso.deploykit.client.common.util.HomeFolder;
import nl.sodeso.deploykit.jboss.deploy.*;
import nl.sodeso.deploykit.service.client.DeployKitServiceClient;
import nl.sodeso.deploykit.service.client.exceptions.ProfileNotFoundException;
import nl.sodeso.deploykit.service.client.exceptions.ProfileRetreivalException;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.util.FailedToWriteProfileException;
import nl.sodeso.deploykit.service.util.ProfileStore;

import java.util.logging.Level;
import java.util.logging.Logger;

import static nl.sodeso.deploykit.client.common.util.ParametersContainer.*;

/**
 * @author Ronald Mathies
 */
public class DeployProfile {

    private static final Logger LOG = Logger.getLogger(DeployProfile.class.getName());

    public static void deploy() {
        String deployKitServiceHost;
        Integer deployKitServicePort;
        String deployKitDeployProfile = null;
        String deployKitDeployProfileVersion;

        try {
            deployKitServiceHost = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, DEPLOYKIT_HOST);
            deployKitServicePort = PropertyConfiguration.getInstance().getIntegerProperty(DOMAIN, DEPLOYKIT_PORT);
            deployKitDeployProfile = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, PROFILE_NAME);
            deployKitDeployProfileVersion = PropertyConfiguration.getInstance().getStringProperty(DOMAIN, PROFILE_VERSION);

            LOG.log(Level.INFO, String.format("DeployKit Service: %s:%d", deployKitServiceHost, deployKitServicePort));
            Profile profile = new DeployKitServiceClient(deployKitServiceHost, deployKitServicePort).findProfile(deployKitDeployProfile, deployKitDeployProfileVersion);

            DeployApplicationResources.deploy(profile);
            DeployDatasourceConfiguration.deploy(profile);
            DeployHttpsConfiguration.deploy(profile);
            DeployHttpConfiguration.deploy(profile);
            DeployApplicationResources.deploy(profile);
            DeployAuthenticationConfiguration.deploy(profile);
            DeployApplication.deploy(profile);

            ProfileStore.write(HomeFolder.getProfilesFolder(), profile);
        } catch (ProfileNotFoundException e) {
            throw new DeployException(ErrorCode.ERR_004, deployKitDeployProfile, e.getMessage());
        } catch (ProfileRetreivalException e) {
            throw new DeployException(ErrorCode.ERR_005, deployKitDeployProfile, e.getMessage());
        } catch (FailedToWriteProfileException e) {
            throw new DeployException(ErrorCode.ERR_006, deployKitDeployProfile, e.getMessage());
        }
    }

}
