package nl.sodeso.deploykit.jboss.deploy;

import nl.sodeso.deploykit.client.common.exception.DeployException;
import nl.sodeso.deploykit.client.common.util.ErrorCode;
import nl.sodeso.deploykit.jboss.cli.JBossCliExecutor;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.model.connectors.Http;

import javax.annotation.Nonnull;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployHttpConfiguration {

    private static final Logger LOG = Logger.getLogger(DeployDatasourceConfiguration.class.getName());

    private static final String CLI_MODIFY_PORT =
            "/socket-binding-group=standard-sockets/socket-binding=http:write-attribute(name=port, value=%d)";

//    private static final String CLI_MODIFY_ADDRESS =
//            "/socket-binding-group=standard-sockets/socket-binding=http:write-attribute(name=bound-address, value=%s)";

    public static void deploy(@Nonnull Profile profile) {
        if (profile.getHttp() != null) {
            Http http = profile.getHttp();
            LOG.log(Level.INFO, "Deploying HTTP configuration '" + http.getLabel() + "'.");

            String modifyHttp = String.format(CLI_MODIFY_PORT, http.getPort());
            if (JBossCliExecutor.execute(modifyHttp) != JBossCliExecutor.EXIT_NORMAL) {
                throw new DeployException(ErrorCode.ERR_015);
            }

//            String modifyBoundAddress = String.format(CLI_MODIFY_ADDRESS, http.getHost());
//            if (JBossCliExecutor.execute(modifyBoundAddress) != JBossCliExecutor.EXIT_NORMAL) {
//                throw new DeployException(ErrorCode.ERR_016);
//            }

            LOG.log(Level.INFO, "HTTP configuration '" + http.getLabel() + "' successfully added to Jetty configuration, deployment of the HTTP confguration is finished.");
        }
    }

}
