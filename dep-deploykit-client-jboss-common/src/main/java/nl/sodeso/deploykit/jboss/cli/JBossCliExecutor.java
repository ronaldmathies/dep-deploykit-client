package nl.sodeso.deploykit.jboss.cli;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.client.common.util.SystemPropertiesContainer;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class JBossCliExecutor {

    private static final Logger LOG = Logger.getLogger(JBossCliExecutor.class.getName());

    public static final int EXIT_NORMAL = 0;

    private static final String CMD_PROCESS = "/bin/bash";
    private static final String CLI_COMMAND = "jboss-cli.sh";
    private static final String CLI_CONNECT_PARAM = "--connect";
    private static final String CLI_COMMAND_PARAM = "--command=%s";

    /**
     * Executes the specified command.
     *
     * @param command the command to execute.
     * @return The complete console string resulting from the command execution.
     */
    public static int execute(@Nonnull  String command) {
        int exitCode = -1;

        StringBuilder output = new StringBuilder("CLI command output:\n");

        try {
            File jbossHomeFolder = PropertyConfiguration.getInstance().getFileProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.JBOSS_HOME, new File("/opt/jboss/wildfly/"));
            File jbossBinFolder = new File(jbossHomeFolder, "/bin/");

            String commandArg = String.format(CLI_COMMAND_PARAM, command);
            LOG.log(Level.INFO,
                    String.format("Executing CLI command: %s %s %s %s",
                            CMD_PROCESS,
                            CLI_COMMAND,
                            CLI_CONNECT_PARAM,
                            commandArg));

            ProcessBuilder processBuilder = new ProcessBuilder(CMD_PROCESS, CLI_COMMAND, CLI_CONNECT_PARAM, commandArg);
            processBuilder.directory(jbossBinFolder);
            processBuilder.redirectErrorStream(true);
            Process process = processBuilder.start();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine())!= null) {
                output.append(line).append("\n");
            }
            LOG.log(Level.INFO, output.toString());

            exitCode = process.waitFor();

            LOG.log(Level.INFO, "CLI command completed, exit code was " + exitCode);
        } catch (IOException | InterruptedException e) {
            LOG.log(Level.SEVERE, "Exception occured during execution of command '" + command + "'.", e);
        }

        return exitCode;
    }

    public static int restartJBoss() {
        return execute(":shutdown(restart=true)");
    }
}
