package nl.sodeso.deploykit.jboss.deploy;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.client.common.exception.DeployException;
import nl.sodeso.deploykit.client.common.util.ErrorCode;
import static nl.sodeso.deploykit.client.common.util.SystemPropertiesContainer.*;
import nl.sodeso.deploykit.jboss.cli.JBossCliExecutor;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.model.connectors.Https;
import org.apache.commons.codec.binary.Base64InputStream;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployHttpsConfiguration {

    private static final Logger LOG = Logger.getLogger(DeployDatasourceConfiguration.class.getName());

    private static final String ENCODING = "UTF-8";

    private static final String CLI_ADD_SECURITY_REALM =
            "/core-service=management/security-realm=WebSocketRealm:add()";

    private static final String CLI_ADD_KEYSTORE =
            "/core-service=management/security-realm=WebSocketRealm/server-identity=ssl:add(" +
                    "keystore-path=/keystore/%s, " +
                    "keystore-relative-to=jboss.home.dir, " +
                    "keystore-password=%s)";

    private static final String CLI_ADD_LISTENER =
            "/subsystem=undertow/server=default-server/https-listener=https:add(socket-binding=https, security-realm=WebSocketRealm)";

    public static void deploy(@Nonnull Profile profile) {
        if (profile.getHttps() != null) {
            try {
                Https https = profile.getHttps();
                LOG.log(Level.INFO, "Deploying SSL/HTTPS configuration '" + https.getLabel() + "'.");

                File jbossLocation = PropertyConfiguration.getInstance().getFileProperty(DOMAIN, JBOSS_HOME);
                if (jbossLocation == null) {
                    throw new DeployException(ErrorCode.ERR_012);
                }

                File keystoreLocation = new File(jbossLocation, "/keystore");
                if (!keystoreLocation.exists()) {
                    if (!keystoreLocation.mkdir()) {
                        throw new DeployException(ErrorCode.ERR_011, keystoreLocation.toString());
                    }
                }

                File jdbcDriverJarFile = new File(keystoreLocation, https.getFilename());
                ByteArrayInputStream inputStream = new ByteArrayInputStream(https.getKeystore().getBytes(Charset.forName(ENCODING)));
                Files.copy(new Base64InputStream(inputStream, false), jdbcDriverJarFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

                if (JBossCliExecutor.execute(CLI_ADD_SECURITY_REALM) != JBossCliExecutor.EXIT_NORMAL) {
                    throw new DeployException(ErrorCode.ERR_014);
                }

                String addKeystore = String.format(CLI_ADD_KEYSTORE,
                        https.getFilename(),
                        https.getKeyStorePassword());
                if (JBossCliExecutor.execute(addKeystore) != JBossCliExecutor.EXIT_NORMAL) {
                    throw new DeployException(ErrorCode.ERR_013);
                }

//                String addListener = String.format(CLI_ADD_LISTENER,
//                        https.getFilename(),
//                        https.getKeyStorePassword());
                if (JBossCliExecutor.execute(CLI_ADD_LISTENER) != JBossCliExecutor.EXIT_NORMAL) {
                    throw new DeployException(ErrorCode.ERR_013);
                }


                LOG.log(Level.INFO, "SSL/HTTPS configuration '" + https.getLabel() + "' successfully added to Jetty configuration, deployment of the SSL/HTTPS confguration is finished.");
            } catch (IOException ignored) {

            }
        }
    }

}
