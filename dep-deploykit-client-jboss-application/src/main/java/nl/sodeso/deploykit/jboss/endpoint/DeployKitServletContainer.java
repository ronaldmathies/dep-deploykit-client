package nl.sodeso.deploykit.jboss.endpoint;

import org.glassfish.jersey.servlet.ServletContainer;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

/**
 * @author Ronald Mathies
 */
@WebServlet(
    name="DeployKit JBoss Service",
    loadOnStartup = 1,
    urlPatterns = { "/*" },
    initParams = {
        @WebInitParam(name=DeployKitServletContainer.INIT_APPLICATION, value="nl.sodeso.deploykit.jboss.endpoint.DeployKitServiceConfig")
    })
public class DeployKitServletContainer extends ServletContainer {

    public static final String INIT_APPLICATION = "javax.ws.rs.Application";

}
