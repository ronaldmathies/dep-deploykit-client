Available variables exposed by the DeployKit JBoss client which can be used
when resources are being processed:

profile.name
profile.version