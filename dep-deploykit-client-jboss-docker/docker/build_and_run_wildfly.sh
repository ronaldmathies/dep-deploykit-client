docker stop deploykit-jboss
docker rm deploykit-jboss
docker rmi deploykit/jboss

docker build -t deploykit/jboss .
docker run --env-file=./environment/env.list -d -p 6080:8080 -p 6443:8443 -p 9990:9990 --name deploykit-jboss deploykit/jboss
